package com.choucair.formacion.steps;

import java.util.List;
import com.choucair.formacion.pageobjects.ColorlibFormValidationPage;
import net.thucydides.core.annotations.Step;

public class ColorlibFormValidationSteps {
	
	ColorlibFormValidationPage colorlibFormValidationPage;
	
	@Step
	public void diligenciar_popup_datos_tabla(List<List<String>> data,int id) {
		
		colorlibFormValidationPage.Required(data.get(id).get(0));
		colorlibFormValidationPage.Select_Sport(data.get(id).get(1));
		colorlibFormValidationPage.Multiple_Select(data.get(id).get(2));
		colorlibFormValidationPage.Multiple_Select(data.get(id).get(3));
		colorlibFormValidationPage.url(data.get(id).get(4));
		colorlibFormValidationPage.email(data.get(id).get(5));
		colorlibFormValidationPage.password(data.get(id).get(6));
		colorlibFormValidationPage.confirm_password(data.get(id).get(7));
		colorlibFormValidationPage.minimun_field_size(data.get(id).get(8));
		colorlibFormValidationPage.maximun_field_size(data.get(id).get(9));
		colorlibFormValidationPage.number(data.get(id).get(10));
		colorlibFormValidationPage.Ip(data.get(id).get(11));
		colorlibFormValidationPage.Date(data.get(id).get(12));
		colorlibFormValidationPage.DateEarlier(data.get(id).get(13));
		colorlibFormValidationPage.validate();
	}
	
	@Step
	public void verificar_ingreso_datos_formulario_exitoso(){
		
		colorlibFormValidationPage.form_sin_errores();
	}
	
	
	@Step
	public void verificar_ingreso_datos_formulario_con_errores(){
		
		colorlibFormValidationPage.form_con_errores();
	}
	
	
	

}

package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.ColorlibLoginPage;
import com.choucair.formacion.pageobjects.ColorlibMenuPage;

import net.thucydides.core.annotations.Step;

//import net.thucydides.core.annotations.Step;

public class PopupValidationSteps {
	
	ColorlibLoginPage colorlibLoginPage;
	ColorlibMenuPage colorlibMenuPage;
	
	
	@Step
	public void login_colorlib(String strUsuario,String strPass) {
		 
		colorlibLoginPage.open();
		colorlibLoginPage.IngresarDatos(strUsuario,strPass);
		
	}
	
	@Step
	public void ingresar_form_validation() {
		
		colorlibMenuPage.menuFormValidation();
		
	}

}